import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatError, MatFormFieldModule } from '@angular/material/form-field';

import { CommonModule } from '@angular/common';
import { ListComponent } from './pages/list/list.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { NgModule } from '@angular/core';
import { TaskComponent } from 'src/app/components/task/task.component';
import { TodoRoutingModule } from './todo-routing.module';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    TaskComponent,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
  ],
})
export class TodoModule {}
