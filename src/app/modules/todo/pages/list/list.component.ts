import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Component } from '@angular/core';
import { ITask } from 'src/app/core/interfaces/i-task';
import { TaskServiceService } from '../../../../core/services/task-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  fTask: FormGroup = this.fb.group({
    name: [null, [Validators.required, Validators.minLength(3)]],
  });
  constructor(
    private fb: FormBuilder,
    public taskService: TaskServiceService
  ) {}

  saveTask() {
    if (this.fTask.invalid) return;
    this.taskService.addTask(this.fTask.value.name);
    this.fTask.reset();
  }
}
