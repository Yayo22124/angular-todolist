import { Component } from '@angular/core';
import { TaskServiceService } from './core/services/task-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TodoList';

  constructor(
    taskSrv: TaskServiceService
  ){
    taskSrv.readListTasksFromLocalStorage();
  }
}
