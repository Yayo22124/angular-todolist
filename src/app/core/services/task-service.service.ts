import { ITask } from '../interfaces/i-task';
import { Injectable } from '@angular/core';
import { LS_LIST_TASK } from '../constants/Storage.constants';

@Injectable({
  providedIn: 'root',
})
export class TaskServiceService {
  private listTasks: ITask[] = [];
  constructor() {}

  addTask(task: string): void {
    this.listTasks.push({
      task,
      status: false,
      create: new Date(),
    });
    this.saveListInLocalStorage();
  }

  removeTask(index: number): void {
    this.listTasks.splice(index, 1);
    this.saveListInLocalStorage()
  }

  getTotalTask(): number {
    return this.listTasks.length
  }

  getEndedTasks(): number {
    return this.listTasks.filter(e => e.status).length;
  }


  getListTasks(): ITask[] {
    return this.listTasks
  }

  saveListInLocalStorage(): void {
    localStorage.setItem(LS_LIST_TASK, JSON.stringify(this.listTasks))
  }

  readListTasksFromLocalStorage(): void {
    const listTasksInString = localStorage.getItem(LS_LIST_TASK);
    if (!listTasksInString) return
    this.listTasks = JSON.parse(listTasksInString)
  }
}
